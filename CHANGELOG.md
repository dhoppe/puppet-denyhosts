## 2015-08-13 Release 1.0.7
### Summary:
- [Beaker] Update Beaker environment
- [RSpec] Update RSpec environment
- [Travis CI] Update Travis CI environment
- [Puppet Forge] Update license, version requirement

## 2015-02-25 Release 1.0.6
### Summary:
- [Beaker] Update Beaker environment
- [Travis CI] Update Travis CI environment

## 2015-02-25 Release 1.0.5
### Summary:
- [Beaker] Update Beaker environment
- [Puppet] Update list of not supported releases

## 2014-12-06 Release 1.0.4
### Summary:
- [Puppet] Update documentation
- [Rspec] Made some changes to the build environment

## 2014-11-09 Release 1.0.3
### Summary:
- [Puppet] Switch to top-scope variables

## 2014-11-09 Release 1.0.2
### Summary:
- [Puppet] Amending attributes

## 2014-11-08 Release 1.0.1
### Summary:
- [Beaker] Update nodesets
- [Beaker] Change exit codes
- [Hiera] Remove double quotes
- [Puppet] Amending attributes

## 2014-11-06 Release 1.0.0
### Summary:
- Generated from [https://github.com/dhoppe/puppet-skeleton-standard](https://github.com/dhoppe/puppet-skeleton-standard)
